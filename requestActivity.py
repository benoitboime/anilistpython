import json

import requests as requests

query = '''
query ($userId_in: [Int], $createdAt_greater: Int) {
  Page(perPage: 1000){
     activities(userId_in: $userId_in, sort:  ID_DESC, createdAt_greater: $createdAt_greater){
      ... on ListActivity {
        status
        progress
        createdAt
        siteUrl
        media {
          title {
            userPreferred
          }
          coverImage {
            extraLarge
          }
          bannerImage
          siteUrl
        }
        user {
          avatar {
            large
          }
          siteUrl
          name
        }
      }
    }
  }
}
'''


def get_activities():
    with open('userId.txt') as user_id_file:
        user_id_data = user_id_file.read().splitlines()

    created_at_greater_file = open("createdAt_greater.txt", "r")
    created_at_greater_data = created_at_greater_file.read()

    created_at_greater_file.close()
    user_id_file.close()

    variables = {
        'userId_in': user_id_data,
        'createdAt_greater': created_at_greater_data,
    }

    url = 'https://graphql.anilist.co'
    response = requests.post(url, json={'query': query, 'variables': variables})
    return response
