import json

import discord
import asyncio
import datetime
from requestActivity import get_activities
from logs import write_logs

client = discord.Client()


async def send_message():
    await client.wait_until_ready()
    channel = client.get_channel(932592244375048242)
    while not client.is_closed():
        response = get_activities()
        activities = json.loads(response.text).get('data').get('Page').get('activities')
        if activities:
            last_created_at = activities[0].get('createdAt')
            created_at_greater_file = open("createdAt_greater.txt", "w")
            created_at_greater_file.write(str(last_created_at))
            created_at_greater_file.close()
            for activity in activities:
                write_logs(activity)
                if activity.get('progress'):
                    embed = discord.Embed(
                        description=activity.get('status') + ' ' + activity.get(
                            'progress') + ' of ' + "[" + activity.get('media').get('title').get(
                            'userPreferred') + "]" + "(" + activity.get('media').get('siteUrl') + ")",
                        color=0x00FF00,
                    )
                else:
                    embed = discord.Embed(
                        description=activity.get('status') + ' ' + "[" + activity.get('media').get('title').get(
                            'userPreferred') + "]" + "(" + activity.get('media').get('siteUrl') + ")",
                        color=0x00FF00,
                    )
                if activity.get('user').get('name') and activity.get('user').get('siteUrl') and activity.get(
                        'user').get('avatar').get('large'):
                    embed.set_author(
                        name=activity.get('user').get('name'),
                        url=activity.get('user').get('siteUrl'),
                        icon_url=activity.get('user').get('avatar').get('large'),
                    )
                if activity.get('media').get('coverImage').get('extraLarge'):
                    embed.set_thumbnail(
                        url=activity.get('media').get('coverImage').get('extraLarge'),
                    )
                if activity.get('media').get('bannerImage'):
                    embed.set_image(
                        url=activity.get('media').get('bannerImage'),
                    )
                if activity.get('createdAt'):
                    date = datetime.datetime.fromtimestamp(activity.get('createdAt'))
                    embed.set_footer(
                        text=date.strftime("%c"),
                    )
                await channel.send(embed=embed)
        await asyncio.sleep(60)


client.loop.create_task(send_message())
client.run("OTMyNTg4NjEzMzQ1MTA3OTY4.YeVK1A.2Y7F6JmmGCq1BPBQa7B6-QB4bgs")
